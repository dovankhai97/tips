sudo apt-get update
sudo apt-get dist-upgrade -y --autoremove

echo "\n\n XONG UPDATE"

sudo apt-get install -y \
	build-essential \
	cmake \
	git \
	wget \
	unzip \
	libavcodec-dev \
	libxvidcore-dev \
	libx264-dev \
	libavresample-dev \
	libavformat-dev \
	libdc1394-22-dev \
	libgstreamer1.0-dev \
	libgtk2.0-dev \
	libatlas-base-dev \
	gfortran \
	libjpeg-dev \
	libpng-dev \
	libswscale-dev \
	libtbb-dev \
	libtbb2 \
	libtiff-dev \
	libv4l-dev \
	pkg-config \
	python-dev \
	python3-dev \
	python-pip \
	python3-pip \
	curl \
	libprotobuf-dev \
	protobuf-compiler \
	libgoogle-glog-dev \
	libgflags-dev \
	libopenblas-dev
	
sudo apt-get install -y \
	libhdf5-serial-dev \
	hdf5-tools \
	libhdf5-dev \
	zlib1g-dev \
	libjpeg8-dev

sudo apt-get install -y \
	graphviz \
	
sudo apt-get install -y \
	libboost-python-dev \
	libboost-thread-dev
	
sudo apt-get install -y powerline


echo "\n\n XONG PIP"

wget https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py
sudo python2 get-pip.py
sudo python get-pip.py

sudo pip3 install virtualenv virtualenvwrapper
sudo rm -rf ~/get-pip.py ~/.cache/pip

echo -e "\n# virtualenv and virtualenvwrapper" >> ~/.bashrc
echo "export WORKON_HOME=$HOME/.virtualenvs" >> ~/.bashrc
echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3" >> ~/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc

cd /package
wget -O opencv-4.1.0.zip https://github.com/opencv/opencv/archive/4.1.0.zip
wget -O opencv_contrib-4.1.0.zip https://github.com/opencv/opencv_contrib/archive/4.1.0.zip

unzip opencv-4.1.0.zip 
unzip opencv_contrib-4.1.0.zip
rm -rf opencv-4.1.0.zip
rm -rf opencv_contrib-4.1.0.zip

cd opencv-4.1.0/
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
	-D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-4.1.0/modules \
	-D CUDA_ARCH_BIN="5.3" \
	-D WITH_GSTREAMER=ON \
	-D WITH_LIBV4L=ON \
	-D OPENCV_GENERATE_PKGCONFIG=YES \ 
	-D WITH_CUDA=ON \
	-D OPENCV_ENABLE_NONFREE=ON ..

echo "\n\n OPENCV"
echo "\n\n ------HAY KIEM TRA-----------"

echo "vao sleep"
sleep 3m

make -j4

echo "vao sleep"
sleep 3m
sudo make install
sudo ldconfig


echo "\n\n NUMBA"
cd /package
wget http://releases.llvm.org/7.0.1/llvm-7.0.1.src.tar.xz
tar -xvf llvm-7.0.1.src.tar.xz
cd llvm-7.0.1.src.tar.xz
rm -rf llvm-7.0.1.src.tar.xz
mkdir llvm_build_dir
cd llvm_build_dir/
cmake ../ -DCMAKE_BUILD_TYPE=Release  -DLLVM_TARGETS_TO_BUILD="ARM;X86;AArch64"
make -j4
echo "vao sleep"
sleep 3m
cd bin/
echo "export LLVM_CONFIG=\""`pwd`"/llvm-config\"" >> ~/.bashrc
echo "alias llvm='"`pwd`"/llvm-lit'" >> ~/.bashrc
source ~/.bashrc
sudo pip3 install llvmlite

cd /package
sudo pip3 install -r requirements_sort.txt
sudo pip3 install -r requirements_tf.txt
sudo pip3 install -r requirements_tf.txt

sudo pip3 install -U numpy grpcio absl-py py-cpuinfo psutil portpicker six mock requests gast h5py astor termcolor protobuf keras-applications keras-preprocessing wrapt google-pasta
sudo pip3 install --pre --extra-index-url https://developer.download.nvidia.com/compute/redist/jp/v42 tensorflow-gpu==1.14.0+nv19.10

echo "\n\n TF"

cd /package
sudo pip install graphviz jupyter
git clone https://github.com/apache/incubator-mxnet.git --branch v1.4.x --recursive
cd incubator-mxnet/
cp make/config.mk .
sed -i 's/USE_CUDA = 0/USE_CUDA = 1/' config.mk
sed -i 's/USE_CUDA_PATH = NONE/USE_CUDA_PATH = \/usr\/local\/cuda/' config.mk
sed -i 's/USE_CUDNN = 0/USE_CUDNN = 1/' config.mk
sed -i '/USE_CUDNN/a CUDA_ARCH := -gencode arch=compute_53,code=sm_53' config.mk
make -j4
echo "vao sleep"
sleep 3m
cd python
sudo python3 setup.py install

#cd ..
#export MXNET_HOME=$(pwd)
#echo "export PYTHONPATH=$MXNET_HOME/python:$PYTHONPATH" >> ~/.bashrc
#source ~/.bashrc