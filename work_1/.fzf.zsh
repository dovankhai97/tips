# Setup fzf
# ---------
if [[ ! "$PATH" == */home/kvdo/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/kvdo/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/kvdo/.fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/home/kvdo/.fzf/shell/key-bindings.zsh"
