# Setup fzf
# ---------
if [[ ! "$PATH" == */home/vnguyen/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/kvdo/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/kvdo/.fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/kvdo/.fzf/shell/key-bindings.bash"
