
# CD to specified folders
alias smpmpro='cd /work/dcao/working/apm-smpmpro'
alias aptiov='cd /work/dcao/working/apm-ami-aptiov'
alias atf='cd /work/dcao/working/apm-atf'
alias bmc='cd /work/dcao/working/apm-bmc-ast2500'
alias build='cd /work/dcao/working/apm-build-scripts'

alias falcon_smpmpro='cd /work/dcao/working/boards/falcon/amp-smpmpro'
alias falcon_aptiov='cd /work/dcao/working/boards/falcon/amp-ami-aptiov'
alias falcon_atf='cd /work/dcao/working/boards/falcon/amp-atf'
alias falcon_bmc='cd /work/dcao/working/boards/falcon/amp-bmc-ast2500'
alias falcon_build='cd /work/dcao/working/boards/falcon/amp-build-scripts'

alias raptor_smpmpro='cd /work/dcao/working/boards/raptor/amp-smpmpro'
alias raptor_aptiov='cd /work/dcao/working/boards/raptor/amp-ami-aptiov'
alias raptor_atf='cd /work/dcao/working/boards/raptor/amp-atf'
alias raptor_bmc='cd /work/dcao/working/boards/raptor/amp-bmc-ast2500'
alias raptor_build='cd /work/dcao/working/boards/raptor/amp-build-scripts'

#common command
alias ..='cd ..;clear;ls;pwd'
alias ...='cd ../..;clear;ls;pwd'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias sw7='ssh -X -Y dcao@10.38.12.21'
alias sw4='ssh -X -Y dcao@10.38.12.20'
alias sw28='ssh -X -Y dcao@10.38.12.28'
alias sw22='ssh -X -Y dcao@10.38.12.53'
alias sw23='ssh -X -Y dcao@10.38.12.54'
alias sw25='sshpass -p amcc1234 ssh -X amcclab@10.38.65.41'
#eclipse
alias eclipse='/work/dcao/eclipse/eclipse &'
