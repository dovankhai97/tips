"----------------------------------------------------------------------------"
" General Settings                                                           "
"----------------------------------------------------------------------------"
filetype plugin indent on               " Filetype func on

set autoread                            " Auto reload file after external command"
set binary                              " Enable binary support"
set nocompatible                        " no vi compat
set hidden                              " Switch tab without needing save current file
set nofoldenable                        " disable folding"
set guifont=Hack                        " Set font
set encoding=utf-8                      " Encoding type
set number                              " Enable line number
set laststatus=2                        " always show the bottom status bar
set showtabline=2
set noshowmode                          " dont show -- INSERT --
set pastetoggle=<F2>                    " Toggle paste mode with F2
set showcmd                             " Show current command
set showmatch                           " Show matching bracket/parenthesis/etc
set title                               " Change terminal title
"set ttyfast                             " Fast terminal

" Completion Box
set wildmenu                            " Open Box for autocomplete command/filename
"set wildmode=list,full

set completeopt=preview " Make completion menu behave like an IDE
set wildmode=longest,full
" Indent
set tabstop=8
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab

" Key sequence timeout
set timeoutlen=0                        " Enable time out
set ttimeoutlen=0                       " decrease ESC delay

" Search
set incsearch                           " Incremental search
"set hlsearch                            " Highlight matches
set ignorecase                          " Case-insensitive search...
set smartcase                           " ...unless search contains uppercase letter

" Temp Files
set nobackup                            " No backup file
set noswapfile                          " No swap file

"----------------------------------------------------------------------------"
" Vundle Plugin                                                              "
"----------------------------------------------------------------------------"
" initialize vundle
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
"" start- all plugins below
Plugin 'VundleVim/Vundle.vim'

" StatusLine
Plugin 'itchyny/lightline.vim'
Plugin 'ap/vim-buftabline'
"Plugin 'taohexxx/lightline-buffer'

" Colorscheme
Plugin 'tomasr/molokai'

" Productivities
Plugin 'airblade/vim-rooter'
Plugin 'ajh17/vimcompletesme'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'Townk/vim-autoclose'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'

" Indent
Plugin  'tpope/vim-sleuth'

" Git
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'

" Syntax Highlighter
Plugin 'compnerd/arm64asm-vim'          " ARM64
Plugin 'alisdair/vim-armasm'            " ARM
Plugin 'ASL'                            " ACPI ASL
Plugin 'justinmk/vim-syntax-extra'      " Improve syntax highlight

" Enhanced search
Plugin 'dyng/ctrlsf.vim'
Plugin 'haya14busa/incsearch.vim'

Plugin 'tpope/vim-vinegar'

Plugin 'scrooloose/nerdtree'

Plugin 'ntpeters/vim-better-whitespace'

Plugin 'mbbill/fencview'
Plugin 's3rvac/autofenc'
"Plugin 'ryanoasis/vim-devicons'

" stop - all plugins above
call vundle#end()

""""""""""""""""""""""""""""""""""""""""""""""""""

"" Appearence \"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Tab navigation like Firefox.
nnoremap <S-Tab> :bprevious<CR>
nnoremap <Tab>  :bnext<CR>
"nnoremap <C-w> :bdelete<CR>

"autocmd FileType netrw unmap <Tab>

"" LightLine \""""""""""""""""""""""""""""""""""""""""""""""""

let g:lightline = {
  \     'colorscheme': 'wombat',
  \     'active': {
  \             'left': [ [ 'mode', 'paste' ], [ 'gitbranch' ], [ 'absolutepath' ] ],
  \             'right': [  [ 'percent' ], [ 'lineinfo' ], ['noet'], ['fileencoding'], ['filetype'] ],
  \     },
  \     'component_function':{
  \             'gitbranch': 'fugitive#head',
  \     },
  \     'component_expand': {
  \             'noet': 'LightlineNoexpandtab',
  \     },
  \ }

function! LightlineNoexpandtab()
    return &expandtab?'| SP '.&shiftwidth:'| TB '.&shiftwidth
endfunction

"␣
"↹

"" buftabline \"""""""""""""""""""""""""""""""""""""""""""""""
let g:buftabline_numbers=2
let g:buftabline_indicators=1

""""""""""""""""""""""""""""""""""""""""""""" GitGutter Config
set updatetime=100

""""""""""""""""""""""""""""""""""""""""""""" ctrlp with ripgrep
if executable('rg')
  set grepprg=rg\ --color=never
  let g:ctrlp_user_command = 'rg %s --files --glob ""'
  let g:ctrlp_use_caching = 0
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" Molokai
syntax on
colorscheme molokai

""""""""""""""""""""""""""""""""""""""""""""""" incsearch
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

"" CtrlSF \""""""""""""""""""""""""""""""""""""""
let g:ctrlsf_auto_focus = {
     \ "at": "start"
     \ }

nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordPath
vmap     <C-F>F <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>o :CtrlSFOpen<CR>
nnoremap <C-F>t :CtrlSFToggle<CR>
inoremap <C-F>t <Esc>:CtrlSFToggle<CR>

""""""""""" netrw
let g:netrw_banner = 0
"let g:netrw_browse_split = 4
"let g:netrw_altv = 1
"let g:netrw_winsize = 25
"augroup ProjectDrawer
"  autocmd!
"  autocmd VimEnter * :Vexplore
"augroup END

"""""""""""""""""""""""""""""""""""""""""""""""""""
if 0
set statusline=
"set statusline+=%#DiffAdd#%{(mode()=='n')?'\ \ NORMAL\ ':''}
"set statusline+=%#DiffChange#%{(mode()=='i')?'\ \ INSERT\ ':''}
"set statusline+=%#DiffDelete#%{(mode()=='r')?'\ \ RPLACE\ ':''}
"set statusline+=%#Cursor#%{(mode(1)=='V')?'\ \ VISUAL\ ':''}
set statusline+=%{'\ '.Fun().'\ '}
set statusline+=%{fugitive#head()!=''?'\ >'.fugitive#head().'\ ':''}
set statusline+=%#Visual#       " colour
set statusline+=\ %n\           " buffer number
set statusline+=%{&paste?'\ PASTE\ ':''}
set statusline+=%{&spell?'\ SPELL\ ':''}
set statusline+=%#CursorIM#     " colour
set statusline+=%{&readonly?'\ \ ':''}                        " readonly flag
set statusline+=%#Cursor#               " colour
set statusline+=%#CursorLine#     " colour
set statusline+=\ %t\                   " short file name
set statusline+=%M                        " modified [+] flag
set statusline+=%=                          " right align
set statusline+=%#CursorLine#   " colour
set statusline+=\ %y\                   " file type
set statusline+=%(\\ %{&modifiable?(&expandtab?'et\ ':'noet\ ').&shiftwidth.'\ ':''}%)
set statusline+=%#CursorIM#     " colour
set statusline+=\ %3l:%-2c\         " line + column
set statusline+=%#Cursor#       " colour
set statusline+=\ %3p%%\                " percentage

let s:mode_map = {
      \     'n': 'NORMAL', 'i': 'INSERT', 'R': 'REPLACE', 'v': 'VISUAL', 'V': 'V-LINE', "\<C-v>": 'V-BLOCK',
      \     'c': 'COMMAND', 's': 'SELECT', 'S': 'S-LINE', "\<C-s>": 'S-BLOCK', 't': 'TERMINAL'
      \   }

function! Fun()
    return get(s:mode_map, mode(), '')
endfunction
endif

""""""""""""""""""""""""""""""""""""""""""
let g:better_whitespace_enabled=1

" NERDTree
let NERDTreeStatusline=""
let g:NERDTreeStatusline = '%#NonText#'

